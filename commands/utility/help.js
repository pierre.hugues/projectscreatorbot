const { PREFIX } = require("../../config.json");

module.exports = {
    name: "help",
    description: "List all commands or info about a specific command",
    args: false,
    usage: '[command name]',
    async execute(message, args) {
        const data = [];

        const { commands } = message.client;

        if(!args.length) {
            data.push('Here\'s a list of all my commands:');
            data.push(commands.map(command => command.name).join(', '));
            data.push(`\nYou can send \`${PREFIX}help [command name]\` to get info on a specific command!`);

            try {
                await message.author.send(data, { split: true })
            } catch (error) {
                console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
                message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
            }
            if (message.channel.type === 'dm') return;
            return message.reply('I\'ve sent you a DM with all my commands!');
        }

        const name = args[0].toLowerCase();
        const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

        if (!command) {
            return message.reply('that\'s not a valid command!');
        }

        data.push(`**Name:** ${command.name}`);

        if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
        if (command.description) data.push(`**Description:** ${command.description}`);
        if (command.usage) data.push(`**Usage:** ${PREFIX}${command.name} ${command.usage}`);

        message.channel.send(data, { split: true });
    }
}