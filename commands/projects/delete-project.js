module.exports = {
    name: "deleteproject",
    description: "Delete the role, category and channels for a project",
    args: true,
    admin: true,
    usage: "<name>",
    guildOnly: true,
    async execute(message, args) {
        // Delete Role
        try {
            await message.guild.roles.cache.find(role => role.name === args[0]).delete();
        } catch (e) {
            console.error("Error deleting role");
        }

        // Delete Category
        try {
            let category = message.guild.channels.cache.find(channel => (channel.type === "category" && channel.name === args[0]));
            for(let channel of category.children.array()) {
                await channel.delete();
            }
            await category.delete();
        } catch (e) {
            console.error("Error deleting category");
        }

        message.reply("Project removed !");
    }
}