module.exports = {
    name: "addtoproject",
    description: "Add users to a project",
    args: true,
    admin: true,
    usage: "<name> <user|users>",
    guildOnly: true,
    async execute(message, args) {
        // Check if admin
        if (!message.member.roles.cache.some(role => config.ADMIN_ROLES.includes(role.name))) {
            message.reply("You need to be an administrator to run this command");
            return;
        }

        if (!args.length > 0 || args[0].startsWith("<@")) {
            return message.reply("You need to specify at least one parameter with the name of the project");
        }

        if(!message.mentions.users.first()) {
            return message.reply("You need to specify the members to add to the project");
        }

        for(let mention of message.mentions.users) {
            await message.guild.member(mention[1].id).roles.add(message.guild.roles.cache.find(role => role.name === args[0]).id);
        }

        message.reply("User(s) added to project !");
    }
}