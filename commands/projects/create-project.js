const config = require("../../config.json");

module.exports = {
    name: "createproject",
    description: "Create the role and the category for a project",
    args: true,
    admin: true,
    usage: "<name> <color>",
    guildOnly: true,
    async execute(message, args) {
        // Create Role
        await message.guild.roles.create({
            data: {
                name: args[0],
                color: args[1] ? args[1] : null,
            },
        });

        // Create Category
        await message.guild.channels.create(args[0], {
            type: "category",
            permissionOverwrites: [
                {
                    id: message.guild.roles.cache.find(role => role.name === "@everyone").id,
                    deny: ["VIEW_CHANNEL"],
                },
                {
                    id: message.guild.roles.cache.find(role => role.name === args[0]).id,
                    allow: ["VIEW_CHANNEL"],
                },
            ]
        });

        // Create channels
        for(let textChannels of config.CHANNELS.TEXT) {
            await message.guild.channels.create(textChannels, {
                type: "text",
                parent: message.guild.channels.cache.find(channel => channel.type === "category" && channel.name === args[0]).id
            });
        }

        for(let voiceChannels of config.CHANNELS.VOCAL) {
            await message.guild.channels.create(voiceChannels, {
                type: "voice",
                parent: message.guild.channels.cache.find(channel => channel.type === "category" && channel.name === args[0]).id
            });
        }

        message.reply("New Project initialized !");
    }
}