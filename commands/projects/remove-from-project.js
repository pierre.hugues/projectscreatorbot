module.exports = {
    name: "removefromproject",
    description: "Remove users from a project",
    args: true,
    admin: true,
    usage: "<name> <user|users>",
    guildOnly: true,
    execute(message, args) {
        // Check if admin
        if (!message.member.roles.cache.some(role => config.ADMIN_ROLES.includes(role.name))) {
            return message.reply("You need to be an administrator to run this command");
        }

        if (!args.length > 0 || args[0].startsWith("<@")) {
            return message.reply("You need to specify at least one parameter with the name of the project");
        }

        if(!message.mentions.users.first()) {
            message.reply("You need to specify the members to remove from the project");
            return;
        }

        for(let mention of message.mentions.users) {
            message.guild.member(mention[1].id).roles.remove(message.guild.roles.cache.find(role => role.name === args[0]).id);
        }

        message.reply("User(s) removed from project !");
    }
}