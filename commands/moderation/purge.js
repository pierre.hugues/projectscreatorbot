module.exports = {
    name: "purge",
    description: "Purge X messages",
    args: true,
    admin: true,
    usage: "<nbMessages>",
    guildOnly: true,
    async execute(message, args) {
        try {
            // Delete X messages
            await message.channel.bulkDelete(args[0])
            message.reply(`${args[0]} messages deleted !`);
        } catch (e) {
            console.error("Error deleting messages");
        }
    }
}