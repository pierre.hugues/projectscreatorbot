const Discord = require("discord.js");
const fs = require("fs");

const { PREFIX, BOT_TOKEN, ADMIN_ROLES } = require("./config.json");
const client = new Discord.Client();

// Get all commands
client.commands = new Discord.Collection();
const commandFolders = fs.readdirSync('./commands');
for (const folder of commandFolders) {
	const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const command = require(`./commands/${folder}/${file}`);
		client.commands.set(command.name, command);
	}
}

client.login(BOT_TOKEN);

client.on("message", async (message) => {
    if(message.author.bot) return;
    if(!message.content.startsWith(PREFIX)) return;
    
    const commandBody = message.content.slice(PREFIX.length);
    const args = commandBody.split(' ');
    const commandName = args.shift().toLowerCase();
    
    if(!client.commands.has(commandName)) return;

    const command = client.commands.get(commandName);

    if(command.guildOnly && message.channel.type === "dm") {
        return message.reply("I can't execute that command inside DMs!");
    }

    if(command.args && !args.length) {
        let reply = "You didn't provide any arguments";

        if(command.usage) {
            reply += `\nThe proper usage would be : \`${PREFIX}${commandName} ${command.usage}\``;
        }

        return message.reply(reply);
    }

    // Check if admin
    if (command.admin && !message.member.roles.cache.some(role => ADMIN_ROLES.includes(role.name))) {
        return message.reply("You need to be an administrator to run this command");
    }

    try {
        command.execute(message, args);
    } catch(error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }
    
});